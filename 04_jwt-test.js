'use strict'

const tokenService = require('./services/token.service');
const moment = require('moment');

//datos para simulacion...
const miPass = 'miContraseña';
const badPass = 'miOtraContraseña';
const usuario = {
    _id: '123456789',
    email: 'iap31@alu.ua.es',
    displayName: 'Ignacio Aramendia',
    password: miPass,
    signUpDate: moment().unix(),
    lastLogin: moment().unix()
};

console.log(usuario);

//Creamos un token
const token = tokenService.creaToken(usuario);
//console.log(token);

//decodificamos

tokenService.decodificaToken(token)
    .then(userId => {
        return console.log(`ID1: ${userId}`);
    })
    .catch(err => console.log(err));

//decodificamos token erroneo
const badToken = 'eyJxxxhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c';
tokenService.decodificaToken(badToken)
    .then(userId => {
        return console.log(`ID2: ${userId}`);
    })
    .catch(err => console.log(err));

