'use strict'

const PassService = require('./services/pass.services');
const moment = require('moment');
const { hash } = require('bcrypt');

 //datos para simulacion...
const miPass = 'miContraseña';
const badPass = 'miOtraContraseña';
const usuario = {
    _id: '123456789',
    email: 'iap31@alu.ua.es',
    displayName: 'Ignacio Aramendia',
    password: miPass,
    signUpDate: moment().unix(),
    lastLogin: moment().unix()
};

console.log(usuario);

//encriptamos el password
PassService.encriptaPassword(usuario.password)
    .then(hash => {
        usuario.password = hash;
        console.log(usuario);

        //verificamos el pass
        PassService.comparaPassword(miPass, usuario.password)
            .then( is0k => {
                if(is0k){
                    console.log('p1: El pass es correcto');
                }
                else{
                    console.log('p1: El pass no es correcto');
                }
            }) 
            .catch(err => console.log(err));


        //verificamos el pass contra un pass falso
        PassService.comparaPassword(badPass, usuario.password)
            .then( is0k => {
                if(is0k){
                    console.log('p2: El pass es correcto');
                }
                else{
                    console.log('p2: El pass no es correcto');
                }
            }) 
            .catch(err => console.log(err));
    });
    