'use strict'

const jwt = require('jwt-simple');
const moment = require('moment');

const SECRET = require('../config').secret;
const EXP_TIME = require('../config').tokenExpTmp;

//creartoken
//devuelve un token tipo jwt
//formato jwt:
// header.payload.verify_signature
//
//Donde:
//      Header (objeto json con )

function creaToken(user){
    const payload = {
        sub: user._id,
        iat: moment().unix(),
        exp: moment().add(EXP_TIME, 'minutes').unix()
    };
    return jwt.encode(payload, SECRET);
};

//decodificaToken 
//
//devuelve el identificador del usuario
//

function decodificaToken(token){
    return new Promise( (resolve, reject) => {
        try{
            const payload = jwt.decode(token, SECRET, true);
            if(payload.exp <= moment().unix()){
                reject({
                    status: 401,
                    message: 'El token ha caducado'
                });
                resolve(payload.sub);
                console.log(payload);
            }
        }catch{
            reject({
                status: 500,
                messge: 'El token no es correcto'
            });
        }
    });
}

module.exports = {
    creaToken,
    decodificaToken
};