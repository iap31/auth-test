'use strict'

const bcrypt = require('bcrypt');

//encriptaPassword
//devuelve un hash con un salt
function encriptaPassword(password){
    return bcrypt.hash(password, 10);
}

// comparaPassword
//devolver verdadero o falso si coinciden o no el pass y el hash

function comparaPassword(password, hash){
    return bcrypt.compare(password, hash);
}

module.exports = {
    encriptaPassword,
    comparaPassword
};

